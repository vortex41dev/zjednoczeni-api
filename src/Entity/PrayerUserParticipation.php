<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PrayerUserParticipationRepository")
 * @ORM\Table(name="prayer_user_participation")
 */
class PrayerUserParticipation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Prayer
     * @ORM\ManyToOne(targetEntity="App\Entity\Prayer", inversedBy="userPrayerAssociations")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $prayer;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userPrayerAssociations")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $target_date;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Prayer
     */
    public function getPrayer(): ?Prayer
    {
        return $this->prayer;
    }

    /**
     * @param Prayer $prayer
     */
    public function setPrayer(Prayer $prayer): void
    {
        $this->prayer = $prayer;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getTargetDate(): ?\DateTimeInterface
    {
        return $this->target_date;
    }

    public function setTargetDate(\DateTimeInterface $target_date): self
    {
        $this->target_date = $target_date;

        return $this;
    }
}
