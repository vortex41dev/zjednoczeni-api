<?php


namespace App\Entity;


class PushNotification
{
    /** @var string */
    public $title;

    /** @var string */
    public $msg;

    /** @var string */
    public $icon;

    /** @var string */
    public $badge;

    /** @var string */
    public $url;

    /** @var array */
    public $actions = [];

    /** @var mixed */
    public $data = null;

    /**
     * PushNotification constructor.
     * @param string $title
     * @param string $msg
     * @param string $icon
     * @param string $badge
     * @param string $url
     * @param array $actions
     */
    public function __construct(string $title, string $msg, string $icon, string $badge, string $url, array $actions = [], $data = null)
    {
        $this->title = $title;
        $this->msg = $msg;
        $this->icon = $icon;
        $this->badge = $badge;
        $this->url = $url;
        $this->actions = $actions;
        $this->data = $data;
    }


}
