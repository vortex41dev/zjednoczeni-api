<?php


namespace App\Entity;


interface WithIdInterface
{
    public function getId();
}
