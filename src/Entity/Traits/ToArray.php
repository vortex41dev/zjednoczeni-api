<?php


namespace App\Entity\Traits;


trait ToArray
{
    public function toArray(): array
    {
        $arr = [];
        $get_object_vars = get_object_vars($this);
        foreach ($get_object_vars as $key => $val) {
            if ($val instanceof \DateTimeInterface) {
                $arr[$key] = $val->format(\DateTime::ATOM);
            } else {
                $arr[$key] = $val;
            }
        }
        return $arr;
    }
}
