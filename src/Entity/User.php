<?php

namespace App\Entity;

use App\Entity\Traits\ToArray;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 */
class User extends BaseUser
{
    use ToArray;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $takesPartInPrayer = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $meetingNotificationsEnabled = true;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string|null
     */
    protected $plainPassword;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PrayerUserParticipation", mappedBy="user", fetch="EXTRA_LAZY")
     */
    protected $userPrayerAssociations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Article", mappedBy="author")
     */
    private $articles;


    public function __construct()
    {
        parent::__construct();
        $this->password = '';
        $this->getGroups();
        $this->articles = new ArrayCollection();
    }

    public function getTakesPartInPrayer(): ?bool
    {
        return $this->takesPartInPrayer;
    }

    public function setTakesPartInPrayer(bool $takesPartInPrayer): self
    {
        $this->takesPartInPrayer = $takesPartInPrayer;

        return $this;
    }

    public function getMeetingNotificationsEnabled(): ?bool
    {
        return $this->meetingNotificationsEnabled;
    }

    public function setMeetingNotificationsEnabled(bool $meetingNotificationsEnabled): self
    {
        $this->meetingNotificationsEnabled = $meetingNotificationsEnabled;

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setAuthor($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            // set the owning side to null (unless already changed)
            if ($article->getAuthor() === $this) {
                $article->setAuthor(null);
            }
        }

        return $this;
    }
}
