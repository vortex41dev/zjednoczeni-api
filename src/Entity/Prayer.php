<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PrayerRepository")
 */
class Prayer extends BasicEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Type("DateTime<'Y-m-d\TH:i:sP'>")
     */
    protected $date_since;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Type("DateTime<'Y-m-d\TH:i:sP'>")
     */
    protected $date_to;

    /**
     * @ORM\Column(type="text")
     */
    protected $announcement_notification_content;

    /**
     * @ORM\Column(type="text")
     */
    protected $remind_notification_content;

    /**
     * @ORM\Column(type="time")
     */
    protected $announcement_notification_time;

    /**
     * @ORM\Column(type="time")
     */
    protected $remind_notification_time;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PrayerUserParticipation", mappedBy="prayer", fetch="EXTRA_LAZY")
     */
    protected $userPrayerAssociations;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDateSince(): ?\DateTimeInterface
    {
        return $this->date_since;
    }

    public function setDateSince(?\DateTimeInterface $date_since): self
    {
        $this->date_since = $date_since;

        return $this;
    }

    public function getDateTo(): ?\DateTimeInterface
    {
        return $this->date_to;
    }

    public function setDateTo(?\DateTimeInterface $date_to): self
    {
        $this->date_to = $date_to;

        return $this;
    }

    public function getAnnouncementNotificationContent(): ?string
    {
        return $this->announcement_notification_content;
    }

    public function setAnnouncementNotificationContent(string $announcement_notification_content): self
    {
        $this->announcement_notification_content = $announcement_notification_content;

        return $this;
    }

    public function getRemindNotificationContent(): ?string
    {
        return $this->remind_notification_content;
    }

    public function setRemindNotificationContent(string $remind_notification_content): self
    {
        $this->remind_notification_content = $remind_notification_content;

        return $this;
    }

    public function getAnnouncementNotificationTime(): ?\DateTimeInterface
    {
        return $this->announcement_notification_time;
    }

    public function setAnnouncementNotificationTime(\DateTimeInterface $announcement_notification_time): self
    {
        $this->announcement_notification_time = $announcement_notification_time;

        return $this;
    }

    public function getRemindNotificationTime(): ?\DateTimeInterface
    {
        return $this->remind_notification_time;
    }

    public function setRemindNotificationTime(\DateTimeInterface $remind_notification_time): self
    {
        $this->remind_notification_time = $remind_notification_time;

        return $this;
    }
}
