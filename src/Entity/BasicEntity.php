<?php


namespace App\Entity;


use App\Entity\Traits\ToArray;

class BasicEntity implements WithIdInterface
{
    use ToArray;

    protected $id;

    public function getId()
    {
        return $this->id;
    }
}
