<?php


namespace App\Domain;


use App\Entity\WithIdInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;

class ValidationErrorException extends UnException
{
    /**
     * @param string $message
     * @param mixed $context
     * @param int $code
     */
    public function __construct(
        string $message,
        $context = [],
        int $code = Response::HTTP_BAD_REQUEST
    )
    {
        $developerMessages = array();
        if ($context instanceof \Traversable) {
            $resultContext = [];
            foreach ($context as $error) {
                if ($error instanceof ConstraintViolation) {
                    $resultContext[] = $this->extractErrors($error->getPropertyPath(), $error->getMessage(), $error);
                }
                if ($error instanceof FormError) {
                    $resultContext[] = ['fieldname' => $error->getOrigin()->getName(), 'message' => $error->getMessage()];
                }
            }

            $context = $resultContext;
            $developerMessages[] = 'error.validation';
        } elseif ($code === Response::HTTP_BAD_REQUEST) {
            $developerMessages[] = 'error.validation';
        }

        $convertContext = [];
        foreach ($context as $index => $error) {
            $iConverted = false;
            foreach ($convertContext as $convertIndex => $convertError) {
                if (isset($convertError['fieldname']) && $error['fieldname']
                    && $convertError['fieldname'] === $error['fieldname']) {
                    if (isset($convertError['message'], $error['message'])) {
                        $convertContext[$convertIndex]['message'] .= $error['message'];
                    }
                    if (isset($error['errors'])) {
                        if (isset($convertError['errors'])) {
                            $convertContext[$convertIndex]['errors'] = array_merge($convertError['errors'], $error['errors']);
                        } else {
                            $convertContext[$convertIndex]['errors'] = $error['errors'];
                        }
                    }
                    $iConverted = true;
                }
            }
            if (!$iConverted) {
                $convertContext[] = $error;
            }
        }

        parent::__construct($code, $message, implode('; ', $developerMessages), $convertContext);
    }

    /**
     * Generates context from propertyPath
     *
     * @param string $propertyPath
     * @param string $message
     * @param mixed $error
     * @return array
     */
    private function extractErrors(string $propertyPath, string $message, $error): array
    {
        $collection = [];
        $root = $error->getRoot();

        $propertyPathArray = explode('.', $propertyPath);
        if (count($propertyPathArray) > 1) {
            array_shift($propertyPathArray);

            $rowErrors = $this->extractErrors(implode('.', $propertyPathArray), $message, $error);

            if (!empty($rowErrors)) {
                $propertyPathArray = preg_split('/[\[\]]+/', $propertyPath, -1, PREG_SPLIT_NO_EMPTY);
                $indexValue = $this->getIndexFromCollection($propertyPathArray[1], $propertyPathArray[0], $root);

                $collection = [
                    'fieldname' => $propertyPathArray[0],
                    'errors' => [['index' => $indexValue, 'errors' => [$rowErrors]]]
                ];
            }
        } else {
            $propertyPathArray = preg_split('/[\[\]]+/', $propertyPath, -1, PREG_SPLIT_NO_EMPTY);
            if (count($propertyPathArray) > 1) {
                $fieldname = $propertyPathArray[0];
                array_shift($propertyPathArray);
                $rowErrors = $this->extractErrors(implode('.', $propertyPathArray), $message, $error);

                if (!empty($rowErrors)) {
                    $collection = [
                        'fieldname' => $fieldname,
                        'errors' => [$rowErrors]
                    ];
                }
            } else {
                $collection = ['fieldname' => $propertyPath, 'message' => $message];
            }
        }

        return $collection;
    }

    /**
     *
     * @param string $indexValue
     * @param string $fieldname
     * @param mixed $entity
     * @return int
     */
    private function getIndexFromCollection(string $indexValue, string $fieldname, $entity): int
    {
        $methodName = 'get'.ucfirst($fieldname);
        if ($entity instanceof WithIdInterface && method_exists($entity, $methodName)) {
            $counter = 0;
            /** @var WithIdInterface $collectionObject */
            foreach ($entity->$methodName() as $collectionObject) {
                if ((string)($collectionObject->getId()) === $indexValue) {
                    $indexValue = $counter;
                    break;
                }
                $counter++;
            }
        }

        return (int)$indexValue;
    }
}
