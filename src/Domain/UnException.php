<?php

namespace App\Domain;

use RuntimeException;

class UnException extends RuntimeException
{
    /** @var string */
    private $developerMessage;

    /** @var array */
    private $context;

    /** @var int */
    private $internalCode;


    /**
     * @param int $code
     * @param string $message
     * @param string $developerMessage
     * @param array $context
     * @param int $internalCode
     * @param \Exception $previous
     */
    public function __construct(
        int $code = 0,
        string $message = '',
        string $developerMessage = '',
        array $context = [],
        int $internalCode = 0,
        \Exception $previous = null
    )
    {
        parent::__construct($message, $code, $previous);

        $this->developerMessage = $developerMessage;
        $this->context = $context;
        $this->internalCode = $internalCode;
    }

    /**
     * Returns developer message
     *
     * @return string
     */
    public function getDeveloperMessage(): string
    {
        return $this->developerMessage;
    }

    /**
     * Returns exception context
     *
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }

    /**
     * Gets internal exception code
     *
     * @return int
     */
    public function getInternalCode(): int
    {
        return $this->internalCode;
    }
}
