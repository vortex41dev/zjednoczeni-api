<?php


namespace App\Services;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Definicja ValidationErrorTransformer do przetwarzania błedów walidacji na wyjątki.
 */
class ValidationErrorTransformer implements IValidationErrorTransformer
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FormInterface $form
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function throwFormErrors(FormInterface $form)
    {
        $message = '';

        $errors = $this->extractErrors($form);
        if ($form->getErrors()->count() > 0) {
            foreach ($form->getErrors() as $formError) {
                if (!empty($formError->getOrigin()->getName())) {
                    $errors[] = [
                        'fieldname' => $formError->getOrigin()->getName(),
                        'message' => $formError->getMessage()
                    ];
                } else {
                    $message = $formError->getMessage();
                }
            }
        }

        throw $this->createException($errors, $message);
    }

    /**
     * @param ConstraintViolationListInterface $errors
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function throwValidatorErrors(ConstraintViolationListInterface $errors)
    {
        $message = '';

        foreach ($errors as $error) {
            $message .= $error->getMessage();
        }

        throw $this->createException([], $message);
    }

    /**
     * @param array $collection Array of validation errors, each item is an array
     * like [ "fieldname" => "fieldname", "message" => "message"]
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function throwValidationErrors(array $collection)
    {
        throw $this->createException($collection);
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    private function extractErrors(FormInterface $form): array
    {
        $collection = [];

        foreach ($form->all() as $fieldname => $field) {
            $rowErrors = $this->extractErrors($field);
            if (!empty($rowErrors)) {
                if ($field->getParent()->getConfig()->getType()->getBlockPrefix() === 'collection') {
                    $row = ['index' => $fieldname, 'errors' => $rowErrors];
                } else {
                    $row = ['fieldname' => $fieldname, 'errors' => $rowErrors];
                }
                $collection[] = $row;
            }

            foreach ($field->getErrors() as $error) {
                $collection[] = ['fieldname' => $fieldname, 'message' => $error->getMessage()];
            }
        }

        return $collection;
    }

    /**
     * Tworzy wyjątek ApiValidationException ze kodem statusu HTTP = 400.
     *
     * @param array $collection
     * @param string $message
     * @return ElsAuthException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    private function createException(array $collection, string $message = ''): ElsAuthException
    {
        $developerMessage = $this->translator->trans('error.validation');
        if (empty($collection) && null === $message) {
            $developerMessage = $this->translator->trans('error.no-validation-errors');
        }

        return new ElsAuthException(
            Response::HTTP_BAD_REQUEST,
            $message,
            $this->translator->trans($developerMessage),
            $collection
        );
    }
}
