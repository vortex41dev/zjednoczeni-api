<?php


namespace App\Services\PrayerAssociation;


use App\Entity\Prayer;
use App\Repository\PrayerUserParticipationRepository;
use App\Repository\UserRepository;

class PrayerUsersChooser implements PrayerUsersChooserInterface
{
    /** @var PrayerUserParticipationRepository */
    private $prayerUserAssociationsRepository;

    /** @var UserRepository */
    private $userRepository;

    /**
     * PrayerUsersAssociator constructor.
     * @param PrayerUserParticipationRepository $prayerUserAssociationsRepository
     * @param UserRepository $userRepository
     */
    public function __construct(PrayerUserParticipationRepository $prayerUserAssociationsRepository, UserRepository $userRepository)
    {
        $this->prayerUserAssociationsRepository = $prayerUserAssociationsRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Prayer $prayer
     * @return array
     * @throws \Exception
     */
    public function chooseParticipants(Prayer $prayer): array
    {
        $allUsersThatTakePart = $this->userRepository->findUsersTakingPartInPrayer();
        $numberOfPeopleToAssociate = floor(count($allUsersThatTakePart) / 7) + (((int)(new \DateTime())->format('w')) < (count($allUsersThatTakePart) % 7) ? 1 : 0);

        if ($numberOfPeopleToAssociate < 1) {
            $numberOfPeopleToAssociate = 1;
        }

        $otherUsers = [];
        for ($i = 7; $i >= 1; $i--) {
            $otherUsers = $this
                ->prayerUserAssociationsRepository
                ->findParticipantsThatDidntPrayInLastDaysYet($prayer, $i);
            if ($otherUsers) {
                break;
            }
        }
        if (!$otherUsers) {
            return [];
        }
        $chosenUsers = [];
        for ($i = 0; $i < $numberOfPeopleToAssociate && count($otherUsers) > 0; $i++) {
            $index = rand(0, count($otherUsers) - 1);
            $chosenUsers[] = $otherUsers[$index];
            array_splice($otherUsers, $index, 1);
        }
        return $chosenUsers;
    }
}
