<?php


namespace App\Services\PrayerAssociation;


use App\Entity\Prayer;
use App\Entity\PrayerUserParticipation;
use App\Entity\User;
use App\Repository\PrayerRepository;
use App\Repository\PrayerUserParticipationRepository;
use App\UseCase\PrayerAssociation\PrayerAssociatedToUserEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class PrayersUsersAssociator
{

    /** @var PrayerRepository */
    private $prayerRepository;

    /** @var PrayerUsersChooser */
    private $prayerUsersAssociator;

    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /** @var PrayerUserParticipationRepository */
    private $prayerUserParticipationRepository;

    /**
     * PrayersUsersAssociator constructor.
     * @param PrayerRepository $prayerRepository
     * @param PrayerUsersChooser $prayerUsersAssociator
     * @param EventDispatcherInterface $eventDispatcher
     * @param PrayerUserParticipationRepository $prayerUserParticipationRepository
     */
    public function __construct(
        PrayerRepository $prayerRepository,
        PrayerUsersChooser $prayerUsersAssociator,
        EventDispatcherInterface $eventDispatcher,
        PrayerUserParticipationRepository $prayerUserParticipationRepository
    )
    {
        $this->prayerRepository = $prayerRepository;
        $this->prayerUsersAssociator = $prayerUsersAssociator;
        $this->eventDispatcher = $eventDispatcher;
        $this->prayerUserParticipationRepository = $prayerUserParticipationRepository;
    }

    /**
     * @return array
     * @throws \Exception
     * @throws \Doctrine\ORM\ORMException
     */
    public function associateUsersToPrayers(): array
    {
        $prayers = $this->prayerRepository->findActivePrayers();

        $allAssociations = [];

        foreach ($prayers as $prayer) {
            $associatedUsers = $this->prayerUsersAssociator->chooseParticipants($prayer);
            $associations = $this->createAssociations($associatedUsers, $prayer);
            $this->dispatchEvents($associations);
            $allAssociations[] = [
                'prayer' => $prayer,
                'associations' => $associations,
            ];
        }
        return $allAssociations;
    }

    /**
     * @param User[] $associatedUsers
     * @param $prayer
     *
     * @return PrayerUserParticipation[]
     *
     * @throws \Doctrine\ORM\ORMException
     */
    protected function createAssociations(array $associatedUsers, Prayer $prayer): array
    {
        $associations = [];
        foreach ($associatedUsers as $associatedUser) {
            $association = new PrayerUserParticipation();
            $association->setPrayer($prayer);
            $association->setUser($associatedUser);
            $association->setTargetDate(new \DateTime());
            $this->prayerUserParticipationRepository->persist($association);
            $associations[] = $association;
        }
        $this->prayerUserParticipationRepository->flush();
        return $associations;
    }

    /**
     * @param PrayerUserParticipation[] $associations
     */
    private function dispatchEvents(array $associations): void
    {
        foreach ($associations as $association) {
            $this->eventDispatcher->dispatch(new PrayerAssociatedToUserEvent($association));
        }
    }

}
