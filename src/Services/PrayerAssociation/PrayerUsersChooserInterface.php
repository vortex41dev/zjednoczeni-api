<?php


namespace App\Services\PrayerAssociation;


use App\Entity\Prayer;
use FOS\UserBundle\Model\UserInterface;

interface PrayerUsersChooserInterface
{
    /**
     * @param Prayer $prayer
     * @return UserInterface[]
     */
    public function chooseParticipants(Prayer $prayer): array;
}
