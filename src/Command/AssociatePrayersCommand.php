<?php

namespace App\Command;

use App\Entity\Prayer;
use App\Entity\PrayerUserParticipation;
use App\Services\PrayerAssociation\PrayersUsersAssociator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AssociatePrayersCommand extends Command
{
    protected static $defaultName = 'app:associate-prayers';

    /** @var PrayersUsersAssociator */
    private $associator;

    /**
     * AssociatePrayersCommand constructor.
     * @param PrayersUsersAssociator $associator
     */
    public function __construct(PrayersUsersAssociator $associator)
    {
        parent::__construct();
        $this->associator = $associator;
    }


    protected function configure()
    {
        $this
            ->setDescription('Associates prayers to users');
//            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
//            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $associations = $this->associator->associateUsersToPrayers();
        foreach ($associations as $association) {
            /** @var Prayer $prayer */
            $prayer = $association['prayer'];
            /** @var PrayerUserParticipation[] $participants */
            $participants = $association['associations'];
            $io->note(
                sprintf(
                    'Associated following participants to %s: %s',
                    $prayer->getName(),
                    implode(',', array_map(
                        static function (PrayerUserParticipation $participation) {
                            return $participation->getUser()->getUsername();
                        }, $participants))
                )
            );
        }

        return 0;
    }
}
