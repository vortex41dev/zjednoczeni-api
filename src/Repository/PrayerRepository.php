<?php

namespace App\Repository;

use App\Entity\Prayer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Prayer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Prayer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Prayer[]    findAll()
 * @method Prayer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrayerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Prayer::class);
    }

    // /**
    //  * @return Prayer[] Returns an array of Prayer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Prayer
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findActivePrayers()
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.date_since <= :now')
            ->andWhere('p.date_to >= :now')
            ->setParameter('now', (new \DateTime())->format(\DateTime::ATOM))
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
