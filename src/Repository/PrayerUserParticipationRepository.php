<?php

namespace App\Repository;

use App\Entity\Prayer;
use App\Entity\PrayerUserParticipation;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PrayerUserParticipation|null find($id, $lockMode = null, $lockVersion = null)
 * @method PrayerUserParticipation|null findOneBy(array $criteria, array $orderBy = null)
 * @method PrayerUserParticipation[]    findAll()
 * @method PrayerUserParticipation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrayerUserParticipationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PrayerUserParticipation::class);
    }

    // /**
    //  * @return PrayerUserParticipation[] Returns an array of PrayerUserParticipation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PrayerUserParticipation
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function persist(PrayerUserParticipation $association)
    {
        $this->getEntityManager()->persist($association);
    }

    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    /**
     * @param Prayer $prayer
     * @param int $daysNr
     * @return User[]
     * @throws \Exception
     */
    public function findParticipantsThatDidntPrayInLastDaysYet(Prayer $prayer, int $daysNr = 7): array
    {
        return $this
            ->getEntityManager()
            ->createQuery('SELECT u FROM App\Entity\User u 
            WHERE 
            u.takesPartInPrayer = 1
            AND
            u.id NOT IN (
                SELECT u2.id
                FROM App\Entity\PrayerUserParticipation pup
                JOIN App\Entity\User u2 WITH pup.user = u2
                WHERE pup.prayer = :prayer
                AND pup.target_date >= :now
            )')
            ->setParameters([
                'prayer' => $prayer,
                'now' => (new \DateTime($daysNr.' days ago'))->format(\DateTime::ATOM),
            ])
            ->getResult();
    }
}
