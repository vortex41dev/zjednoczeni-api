<?php

namespace App\Repository;

use App\Entity\PushSubscriber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PushSubscriber|null find($id, $lockMode = null, $lockVersion = null)
 * @method PushSubscriber|null findOneBy(array $criteria, array $orderBy = null)
 * @method PushSubscriber[]    findAll()
 * @method PushSubscriber[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PushSubscriberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PushSubscriber::class);
    }

    // /**
    //  * @return PushSubscriber[] Returns an array of PushSubscriber objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PushSubscriber
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param PushSubscriber $subscription
     * @throws \Doctrine\ORM\ORMException
     */
    public function insert(PushSubscriber $subscription): void
    {
        $this->getEntityManager()->persist($subscription);
    }

    public function remove(PushSubscriber $subscription): void
    {
        $this->getEntityManager()->remove($subscription);
    }

    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }
}
