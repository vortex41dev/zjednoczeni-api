<?php

namespace App\Form\Type;

use App\Entity\Prayer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrayerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('date_since', TextType::class)
            ->add('date_to', TextType::class)
            ->add('announcement_notification_content')
            ->add('remind_notification_content')
            ->add('announcement_notification_time', TextType::class)
            ->add('remind_notification_time', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Prayer::class,
        ]);
    }
}
