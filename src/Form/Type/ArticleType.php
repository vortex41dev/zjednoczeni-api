<?php

namespace App\Form\Type;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('body', TextType::class)
            ->add('creation_date', DateTimeType::class, [
                'property_path' => 'creationDate',
                'widget' => 'single_text',
//                'format' => \DateTime::ATOM,
//                'format' => "yyyy-MM-dd'T'HH:mm:ssP",
//                'date_format' => "yyyy-MM-dd'T'HH:mm:ssP",
//                'input_format' => "yyyy-MM-dd'T'HH:mm:ssP",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
