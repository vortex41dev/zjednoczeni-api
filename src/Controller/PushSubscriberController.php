<?php


namespace App\Controller;


use App\Form\Type\PushSubscriberType;
use App\UseCase\PushNotifications\AddSubscriberCommand;
use App\UseCase\PushNotifications\CancelSubscriberCommand;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;

class PushSubscriberController extends RestController
{
    /**
     *
     * @SWG\Response(
     *   response=200,
     *   description="Returns web push public key",
     *   @SWG\Schema(
     *     type="object",
     *     @SWG\Property(property="publicKey", type="string")
     *   ),
     * )
     * @SWG\Tag(name="Push Notification subscribers")
     * @Security(name="Bearer")
     *
     * @Rest\Get("api/web-push/public-key")
     */
    public function getSubscriptionPublicKeyAction(): array
    {
        return ['publicKey' => $this->getParameter('push_public_key')];
    }

    /**
     *
     * @SWG\Response(
     *   response=201,
     *   description="Adds push notification subscriber",
     *   @SWG\Schema(ref=@Model(type=\App\Entity\PushSubscriber::class)),
     * )
     * @SWG\Tag(name="Push Notification subscribers")
     * @Security(name="Bearer")
     * @Rest\Post("api/push-subscribers", format="json")
     * @param Request $request
     * @return mixed
     */
    public function postSubscriptionAction(Request $request)
    {
        $command = new AddSubscriberCommand();

        $this->processCommand($request, $command, PushSubscriberType::class);

        return $command->getCommandResult();
    }


    /**
     *
     * @SWG\Response(
     *   response=204,
     *   description="Cancels push notification subscription",
     * )
     * @SWG\Tag(name="Push Notification subscribers")
     * @Security(name="Bearer")
     *
     * @Rest\Delete("api/push-subscribers/{id<\d+>}")
     * @Rest\View(statusCode=204)
     * @param int $id
     */
    public function deleteSubscriptionAction(int $id): void
    {
        $command = new CancelSubscriberCommand($id);

        $this->get('command_bus')->handle($command);
    }
}
