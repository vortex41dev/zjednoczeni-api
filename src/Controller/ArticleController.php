<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\Type\ArticleType;
use App\Repository\ArticleRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends AbstractFOSRestController
{

    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * @var \JMS\Serializer\SerializerInterface
     */
    private $serializer;

    public function __construct(ArticleRepository $articleRepository, SerializerInterface $serializer)
    {
        $this->articleRepository = $articleRepository;
        $this->serializer = $serializer;
    }

    /**
     * @FOSRest\Get("/api/article/{id}", requirements={
     *   "id": "[0-9]+"
     * })
     * @SWG\Response(
     *   response=200,
     *   description="Returns article",
     * )
     * @SWG\Tag(name="Article")
     *
     * @param int $id
     * @return Response
     */
    public function getArticle(int $id): Response
    {
        $article = $this->articleRepository->find($id);
        return $this->handleView($this->view($article, Response::HTTP_OK));
    }

    /**
     * @FOSRest\Get("/api/article")
     * @SWG\Response(
     *   response=200,
     *   description="Returns articles",
     * )
     * @SWG\Tag(name="Article")
     *
     * @return Response
     */
    public function getArticles(): Response
    {
        $articles = $this->articleRepository->findBy([], ['creationDate' => 'DESC']);
        return $this->handleView($this->view($articles, Response::HTTP_OK));
    }

    /**
     * @FOSRest\Post("/api/article")
     * @IsGranted("ROLE_ADMIN")
     * @SWG\Tag(name="Article")
     * @SWG\Response(
     *   response=202,
     *   description="Returns created article",
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createArticle(Request $request): Response
    {
        $article = new Article();
        $article->setAuthor($this->getUser());
        return $this->updateArticleData($request, $article);
    }

    /**
     * @FOSRest\Put("/api/article/{id}")
     * @IsGranted("ROLE_ADMIN")
     *
     * @SWG\Response(
     *   response=202,
     *   description="Returns updated article",
     * )
     * @SWG\Tag(name="Article")
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function updateArticle(Request $request, int $id): Response
    {
        $article = $this->articleRepository->find($id);
        return $this->updateArticleData($request, $article);
    }

    /**
     * @FOSRest\Delete("/api/article/{id}")
     * @IsGranted("ROLE_ADMIN")
     *
     * @SWG\Response(
     *   response=204,
     *   description="Removes article data",
     * )
     * @SWG\Tag(name="Article")
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function deleteArticle(Request $request, int $id): Response
    {
        $article = $this->articleRepository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($article);
        $em->flush();
        return new Response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @param Article|null $article
     * @return JsonResponse|Response
     */
    protected function updateArticleData(Request $request, ?Article $article)
    {
        $id = $article->getId();
        $form = $this->createForm(ArticleType::class, $article, ['allow_extra_fields' => true]);

        $content = $request->getContent();
        /** @var Article $data */
        $data = json_decode($content, true);
        $data['creation_date'] = (new \DateTime($data['creation_date']))->format('Y-m-d H:i:s');
        $form->submit($data);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($article);
                $em->flush();

                return $this->handleView($this->view($article, $id ? Response::HTTP_ACCEPTED : Response::HTTP_CREATED));
            }
            return $this->handleView($this->view(['form' => $form], Response::HTTP_BAD_REQUEST));
        }
        return new JsonResponse([
            'errors' => ['not enough data']
        ], Response::HTTP_BAD_REQUEST);
    }
}
