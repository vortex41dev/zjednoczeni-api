<?php


namespace App\Controller;


use App\UseCase\Command;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;

class RestController extends AbstractFOSRestController
{
    protected function emFlush()
    {
        $this->getDoctrine()->getManager()->flush();
    }

    /**
     * @param Request $request
     * @param Command $command
     * @param string $formType
     */
    protected function processCommand(Request $request, Command $command, string $formType = '')
    {
        if ($request->getMethod() !== 'PATCH') {
            $this->submitForm($request, $command, $formType);
        }
        $this->handleCommand($command);
    }

    /**
     * @param Request $request
     * @param Command $command
     * @param string $formType
     *
     * @throws \AppBundle\Domain\ElsAuthException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     */
    private function submitForm(Request $request, Command $command, string $formType)
    {
        $form = $this->createForm($formType, $command, ['allow_extra_fields' => true]);
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            $this->get('validation_error_transformer')->throwFormErrors($form);
        }
    }

    /**
     * @param Command $command
     */
    protected function handleCommand(Command $command)
    {
        $this->get('command_bus')->handle($command);
    }
}
