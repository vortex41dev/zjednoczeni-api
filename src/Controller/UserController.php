<?php


namespace App\Controller;


use App\Entity\User;
use App\Form\Type\UserFormType;
use App\Form\Type\UserRegistrationFormType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class UserController
 * @package App\Controller
 */
class UserController extends AbstractFOSRestController
{
    /** @var UserManagerInterface */
    private $userManager;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var \JMS\Serializer\SerializerInterface */
    private $serializer;

    public function __construct(UserManagerInterface $userManager, TokenStorageInterface $tokenStorage, SerializerInterface $serializer)
    {
        $this->userManager = $userManager;
        $this->tokenStorage = $tokenStorage;
        $this->serializer = $serializer;
    }

    /**
     * @FOSRest\Get("/api/current-user")
     *
     * @param Request $request
     * @return Response
     */
    public function currentUser(Request $request): Response
    {
        $user = $this->tokenStorage->getToken()->getUser();
        return $this->handleView($this->view($user, Response::HTTP_OK));
    }


    /**
     * @FOSRest\Post("/user")
     *
     * @param Request $request
     * @return Response
     */
    public function createUser(Request $request): Response
    {
        $user = $this->userManager->createUser();
        $user->setEnabled(true);
        $form = $this->createForm(UserRegistrationFormType::class, $user);

        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        return $this->handleUserForm($form, $user);
    }

    /**
     * @FOSRest\Get("/api/user/{id}")
     *
     * @param int $id
     * @return Response
     */
    public function getUserData(int $id): Response
    {
        $this->userIsAdminOrEditsOwnDataFirewall($id);

        $user = $this->userManager->findUserBy(['id' => $id]);
        if (!$user) {
            throw $this->createNotFoundException('error.user.not_found');
        }
        return $this->handleView($this->view($user));
    }

    /**
     * @FOSRest\Put("/api/user/{id}")
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function updateUserData(int $id, Request $request): Response
    {
        $user = $this->userManager->findUserBy(['id' => $id]);
        $this->userIsAdminOrEditsOwnDataFirewall($id);
        if (!$user) {
            throw $this->createNotFoundException('error.user.not_found');
        }

        // need to leave 'getGroups' call - it creates missing groups Array collection.
        $user->getGroups();
        $form = $this->createForm(UserFormType::class, $user);

        /** @var User $data */
        $data = $this->serializer->deserialize($request->getContent(), User::class, 'json');
        $form->submit($data->toArray());

        return $this->handleUserForm($form, $user);
    }

    /**
     * @FOSRest\Post("/api/user/{id}/validation")
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function validateUserData(int $id, Request $request): Response
    {
        $user = $this->userManager->findUserBy(['id' => $id]);
        if (!$user) {
            throw $this->createNotFoundException('error.user.not_found');
        }

        $data = json_decode($request->getContent(), true);

        $errors = [];

        if (isset($data['username'])) {
            $username = $data['username'];
            $usernameErr = $this->validateUsername($user, $username);
            if ($usernameErr) {
                $errors['username'] = $usernameErr;
            }
        }
        if (isset($data['email'])) {
            $email = $data['email'];
            $emailErr = $this->validateEmail($user, $email);
            if ($emailErr) {
                $errors['email'] = $emailErr;
            }
        }

        if ($errors) {
            return new JsonResponse(array_filter($errors), Response::HTTP_BAD_REQUEST);
        }
        return new JsonResponse(true);
    }

    /**
     * @FOSRest\Get("/api/users")
     *
     * @return Response
     */
    public function getUsers(): Response
    {
        $users = $this->userManager->findUsers();
        return $this->handleView($this->view($users));
    }

    /**
     * @param FormInterface $form
     * @param UserInterface|null $user
     * @return JsonResponse|Response
     */
    protected function handleUserForm(FormInterface $form, UserInterface $user)
    {
        $userId = $user->getId();
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->userManager->updateUser($user);

                return $this->handleView($this->view($user, $userId ? Response::HTTP_ACCEPTED : Response::HTTP_CREATED));
            }
            return $this->handleView($this->view(['form' => $form], Response::HTTP_BAD_REQUEST));
        }
        $errors = ['not enough data'];
        return new JsonResponse(['errors' => $errors], Response::HTTP_BAD_REQUEST);
    }

    private function validateUsername(UserInterface $user, string $username): ?string
    {
        if ($user->getUsername() === $username) {
            return null;
        }
        if (!$username) {
            return 'To pole jest wymagane.';
        }
        $user = $this->userManager->findUserByUsername($username);
        if (!$user) {
            return null;
        }
        return 'Ta nazwa użytkownika jest wykorzystywana przez innego użytkownika.';
    }

    private function validateEmail(UserInterface $user, string $email): ?string
    {
        $email = strtolower($email);
        if (strtolower($user->getEmail()) === $email) {
            return null;
        }
        if (!$email) {
            return 'To pole jest wymagane.';
        }
        $user = $this->userManager->findUserByEmail($email);
        if (!$user) {
            return null;
        }
        return 'Ten email jest wykorzystywany przez innego użytkownika.';
    }

    private function userIsAdminOrEditsOwnDataFirewall(int $id): void
    {
        /** @var UserInterface $user */
        $user = $this->tokenStorage->getToken()->getUser();
        if ((int)$user->getId() === $id || $user->hasRole('ROLE_ADMIN')) {
            return;
        }
        throw new UnauthorizedHttpException('error.access.denied');
    }

}
