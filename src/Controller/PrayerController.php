<?php

namespace App\Controller;

use App\Entity\Prayer;
use App\Form\Type\PrayerType;
use App\Repository\PrayerRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\UserBundle\Model\UserManagerInterface;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Swagger\Annotations as SWG;

class PrayerController extends AbstractFOSRestController
{
    /** @var UserManagerInterface */
    private $userManager;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * @var PrayerRepository
     */
    private $prayerRepository;

    /**
     * @var \JMS\Serializer\SerializerInterface
     */
    private $serializer;

    public function __construct(UserManagerInterface $userManager, TokenStorageInterface $tokenStorage, PrayerRepository $prayerRepository, SerializerInterface $serializer)
    {
        $this->userManager = $userManager;
        $this->tokenStorage = $tokenStorage;
        $this->prayerRepository = $prayerRepository;
        $this->serializer = $serializer;
    }

    /**
     * @FOSRest\Get("/api/prayer/{id}", requirements={
     *   "id": "[0-9]+"
     * })
     * @SWG\Response(
     *   response=200,
     *   description="Returns prayer",
     * )
     * @SWG\Tag(name="Prayer")
     *
     * @param int $id
     * @return Response
     */
    public function getPrayer(int $id): Response
    {
        $prayer = $this->prayerRepository->find($id);
        return $this->handleView($this->view($prayer, Response::HTTP_OK));
    }

    /**
     * @FOSRest\Get("/api/prayer")
     * @SWG\Response(
     *   response=200,
     *   description="Returns prayers",
     * )
     * @SWG\Tag(name="Prayer")
     *
     * @return Response
     */
    public function getPrayers(): Response
    {
        $prayer = $this->prayerRepository->findAll();
        return $this->handleView($this->view($prayer, Response::HTTP_OK));
    }

    /**
     * @FOSRest\Post("/api/prayer")
     * @IsGranted("ROLE_ADMIN")
     * @SWG\Tag(name="Prayer")
     * @SWG\Response(
     *   response=202,
     *   description="Returns created prayer",
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createPrayer(Request $request): Response
    {
        $prayer = new Prayer();
        return $this->updatePrayerData($request, $prayer);
    }

    /**
     * @FOSRest\Put("/api/prayer/{id}")
     * @IsGranted("ROLE_ADMIN")
     *
     * @SWG\Response(
     *   response=202,
     *   description="Returns updated prayer",
     * )
     * @SWG\Tag(name="Prayer")
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function updatePrayer(Request $request, int $id): Response
    {
        $prayer = $this->prayerRepository->find($id);
        return $this->updatePrayerData($request, $prayer);
    }

    /**
     * @FOSRest\Delete("/api/prayer/{id}")
     * @IsGranted("ROLE_ADMIN")
     *
     * @SWG\Response(
     *   response=204,
     *   description="Removes prayer data",
     * )
     * @SWG\Tag(name="Prayer")
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function deletePrayer(Request $request, int $id): Response
    {
        $prayer = $this->prayerRepository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($prayer);
        $em->flush();
        return new Response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @param Prayer|null $prayer
     * @return JsonResponse|Response
     */
    protected function updatePrayerData(Request $request, ?Prayer $prayer)
    {
        $id = $prayer->getId();
        $form = $this->createForm(PrayerType::class, $prayer, ['allow_extra_fields' => true]);

        /** @var Prayer $data */
        $data = $this->serializer->deserialize($request->getContent(), Prayer::class, 'json');
        $form->submit($data->toArray());

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($prayer);
                $em->flush();

                return $this->handleView($this->view($prayer, $id ? Response::HTTP_ACCEPTED : Response::HTTP_CREATED));
            }
            return $this->handleView($this->view(['form' => $form], Response::HTTP_BAD_REQUEST));
        }
        return new JsonResponse([
            'errors' => ['not enough data']
        ], Response::HTTP_BAD_REQUEST);
    }
}
