<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191112233111 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE prayer_user_participation (id INT AUTO_INCREMENT NOT NULL, prayer_id INT NOT NULL, user_id INT NOT NULL, target_date DATETIME NOT NULL, INDEX IDX_C9830EC6F0D7D6C6 (prayer_id), INDEX IDX_C9830EC6A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE prayer_user_participation ADD CONSTRAINT FK_C9830EC6F0D7D6C6 FOREIGN KEY (prayer_id) REFERENCES prayer (id)');
        $this->addSql('ALTER TABLE prayer_user_participation ADD CONSTRAINT FK_C9830EC6A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE prayer_user_participation');
    }
}
