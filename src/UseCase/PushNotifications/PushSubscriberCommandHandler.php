<?php

namespace App\UseCase\PushNotifications;


use App\Entity\PushSubscriber;
use App\Repository\PushSubscriberRepository;


class PushSubscriberCommandHandler
{

    /** @var PushSubscriberRepository */
    private $pushSubscribersRepository;

    /**
     * PushSubscriberCommandHandler constructor.
     * @param PushSubscriberRepository $pushSubscribersRepository
     */
    public function __construct(PushSubscriberRepository $pushSubscribersRepository)
    {
        $this->pushSubscribersRepository = $pushSubscribersRepository;
    }

    /**
     * @param AddSubscriberCommand $command
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(AddSubscriberCommand $command): void
    {
        $subscription = new PushSubscriber();
        $subscription->setAuth($command->auth);
        $subscription->setP256dh($command->p256dh);
        $subscription->setEndpoint($command->endpoint);
        $this->pushSubscribersRepository->insert($subscription);
        $this->pushSubscribersRepository->flush();
        $command->setCommandResult($subscription);
    }

    /**
     * @param CancelSubscriberCommand $command
     */
    public function cancel(CancelSubscriberCommand $command): void
    {
        $subscription = $this->pushSubscribersRepository->find($command->id);
        if ($subscription) {
            $this->pushSubscribersRepository->remove($subscription);
            $this->pushSubscribersRepository->flush();
        }
    }

}
