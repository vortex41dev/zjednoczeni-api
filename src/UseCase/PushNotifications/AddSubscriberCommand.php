<?php


namespace App\UseCase\PushNotifications;


use App\UseCase\Command;

class AddSubscriberCommand extends Command
{
    /** @var string */
    public $endpoint;

    /** @var string */
    public $p256dh;

    /** @var string */
    public $auth;

    /**
     * The name of this particular type of message.
     *
     * @return string
     */
    public static function messageName()
    {
        return 'push_subscriber.add';
    }
}
