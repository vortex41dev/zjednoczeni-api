<?php

namespace App\UseCase\PushNotifications;


use App\Entity\PushSubscriber;
use App\UseCase\Command;

class SendPushNotificationCommand extends Command
{
    /** @var PushSubscriber */
    private $subscriber;

    /** @var PushNotification */
    private $pushNotification;

    /**
     * PushNotificationSendCommand constructor.
     * @param PushSubscriber $subscriber
     * @param PushNotification $pushNotification
     */
    public function __construct(PushSubscriber $subscriber, PushNotification $pushNotification)
    {
        $this->subscriber = $subscriber;
        $this->pushNotification = $pushNotification;
    }

    /**
     * @return PushSubscriber
     */
    public function getSubscriber(): PushSubscriber
    {
        return $this->subscriber;
    }

    /**
     * @return PushNotification
     */
    public function getPushNotification(): PushNotification
    {
        return $this->pushNotification;
    }

    /**
     * The name of this particular type of message.
     *
     * @return string
     */
    public static function messageName()
    {
        return 'notification.send';
    }
}
