<?php

namespace App\UseCase\PushNotifications;

use App\UseCase\Command;

class CancelSubscriberCommand extends Command
{
    /** @var int */
    public $id;

    /**
     * CancelSubscriberCommand constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * The name of this particular type of message.
     *
     * @return string
     */
    public static function messageName()
    {
        return 'push_subscriber.cancel';
    }
}
