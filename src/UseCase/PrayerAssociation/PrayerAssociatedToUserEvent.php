<?php


namespace App\UseCase\PrayerAssociation;

use App\Entity\PrayerUserParticipation;
use Symfony\Contracts\EventDispatcher\Event;

class PrayerAssociatedToUserEvent extends Event
{
    public const NAME = 'prayer.user.associated';

    /** @var PrayerUserParticipation */
    private $participation;

    /**
     * PrayerAssociatedToUserEvent constructor.
     * @param PrayerUserParticipation $participation
     */
    public function __construct(PrayerUserParticipation $participation)
    {
        $this->participation = $participation;
    }

    /**
     * @return PrayerUserParticipation
     */
    public function getParticipation(): PrayerUserParticipation
    {
        return $this->participation;
    }
}
