<?php


namespace App\UseCase;


use SimpleBus\Message\Name\NamedMessage;

abstract class Command implements NamedMessage
{
    private $_result;

    public function setCommandResult($result)
    {
        $this->_result = $result;
    }

    public function getCommandResult()
    {
        return $this->_result;
    }
}
