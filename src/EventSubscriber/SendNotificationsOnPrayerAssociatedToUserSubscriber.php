<?php

namespace App\EventSubscriber;

use App\Entity\Prayer;
use App\UseCase\PrayerAssociation\PrayerAssociatedToUserEvent;
use BenTools\WebPushBundle\Model\Message\PushNotification;
use BenTools\WebPushBundle\Model\Subscription\UserSubscriptionManagerRegistry;
use BenTools\WebPushBundle\Sender\PushMessageSender;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SendNotificationsOnPrayerAssociatedToUserSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserSubscriptionManagerRegistry
     */
    private $userSubscriptionManager;

    /**
     * @var PushMessageSender
     */
    private $sender;

    /**
     * NotificationSender constructor.
     * @param UserSubscriptionManagerRegistry $userSubscriptionManager
     * @param PushMessageSender $sender
     */
    public function __construct(
        UserSubscriptionManagerRegistry $userSubscriptionManager,
        PushMessageSender $sender
    )
    {
        $this->userSubscriptionManager = $userSubscriptionManager;
        $this->sender = $sender;
    }


    public function onPrayerAssociated(PrayerAssociatedToUserEvent $event): void
    {
        $participation = $event->getParticipation();

        $user = $participation->getUser();
        $prayer = $participation->getPrayer();

        $subscriptions = $this->userSubscriptionManager->findByUser($user);

        $notification = $this->preparePushNotification($prayer);
        $responses = $this->sender->push($notification->createMessage(), $subscriptions);

        foreach ($responses as $response) {
            echo 'Got subscription: '.$response->getStatusCode()."\r\n";
            if ($response->isExpired()) {
                $this->userSubscriptionManager->delete($response->getSubscription());
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            PrayerAssociatedToUserEvent::class => 'onPrayerAssociated',
        ];
    }

    /**
     * @param Prayer $prayer
     * @return PushNotification
     */
    protected function preparePushNotification(Prayer $prayer): PushNotification
    {
        return new PushNotification('Zjednoczeni w Duchu proszą o modlitwę: '.$prayer->getName(), [
            PushNotification::BODY => $prayer->getAnnouncementNotificationContent(),
            PushNotification::ICON => '/assets/icon_success.png',
        ]);
    }
}
